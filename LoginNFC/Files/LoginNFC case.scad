$fn=20;
D = 50;
H = 30;
W = 55;
T = 3;
R = 1;
lip =5;

pcbT = 1.8;
antennaX = 16.5;
antennaCover = 1;

D2 = 38;

cw=6;

module main() {

	difference() {
		union() {
			minkowski() {
				cubeAt(W,H,D,0,0,0);
				// hemisphere
				difference() {
					sphere(r=T,center=true);
					cylinder(r=T,h=T);
				}
			}
		}

		//Clip notches
		cubeAt(cw,1,5,(W-cw)/2,H,40);
		cubeAt(cw,1,5,(W-cw)/2,-1,40);
	
		//inner void
		cubeAt(W,H,D+lip+1,0,0,0);

		//PCB slot
		cubeAt(55+3.6,pcbT,D+lip,-1.8,10,0);

		antennaCutout();

	};

	difference() {
		cubeAt(14,5,30,antennaX-14/2,0,0);
		// PCB slot
		cubeAt(11.2,pcbT,D+lip,antennaX-11.2/2,0.5,0);
		antennaCutout();
	}
}

module antennaCutout() {
	cubeAt(10,20,D+lip,antennaX-10/2,-T+antennaCover,0);
}

module lid() {

	difference() {
		union() {
			minkowski() {
				cubeAt(W,H,D2,0,-H,0);
				// hemisphere
				difference() {
					sphere(r=T,center=true);
					cylinder(r=T,h=T);
				}
			}
		};
	
		//Clip notches
//		cubeAt(cw,1,5,(W-cw)/2,H,40);
//		cubeAt(cw,1,5,(W-cw)/2,-1,40);
	
		//inner void
		cubeAt(W,H,D+lip+1,0,-H,0);

		//PCB slot
		cubeAt(55+3.6,pcbT,D+lip,-1.8,-10-pcbT,0);

		// antenna
		cubeAt(10,T,10,antennaX-10/2,-antennaCover,D2-10);

		// USB
		cubeAt(14,10,T,W-15,-10-10+1,-T);

		// Magnet mounts
		translate([8,-H+1,30])
			rotate([90,0,0])
				cylinder(r=2.5,h=T+2);

		translate([W-8,-H+1,30])
			rotate([90,0,0])
				cylinder(r=2.5,h=T+2);

	}


	// Corner posts
	cubeAt(3,3,10,0,-3,D2-5);
	cubeAt(3,3,10,0,-H,D2-5);
	cubeAt(3,3,10,W-3,-3,D2-5);
	cubeAt(3,3,10,W-3,-H,D2-5);

		//Clips
	cubeAt(cw,2,13,(W-cw)/2,-H,D2-5);
	cubeAt(cw,.5,2.5,(W-cw)/2,-0.5-H,D2+5.5);
	cubeAt(cw,2,13,(W-cw)/2,-2,D2-5);
	cubeAt(cw,.5,2.5,(W-cw)/2,0,D2+5.5);
	
}

module cubeAt(sx,sy,sz,px,py,pz) {
		translate([px,py,pz])
			cube(size=[sx,sy,sz],center=false);
}

// main();
lid();