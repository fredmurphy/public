/*
 * LoginNFC 0xFRED 2014
 *
 * With help from:
 * 	TRF7970ABP_Demo
 * ---------------------------------------------------------------------------*/

#include <string.h>
#include "driverlib.h"

#include "USB_config/descriptors.h"
#include "USB_API/USB_Common/device.h"
#include "USB_API/USB_Common/usb.h"                     //USB-specific functions
#include "USB_API/USB_CDC_API/UsbCdc.h"
#include "USB_app/usbConstructs.h"

#include "hal.h"
#include "types.h"
#include "USB_app/keyboard.h"
#include "USB_app/keyboardplus.h"
#include "cdcSerial.h"
#include "Hardware/trf7970.h"
#include "Hardware/trf7970BoosterPack.h"

// HID
volatile uint8_t button1Pressed;
volatile uint8_t button2Pressed;
volatile uint8_t keySendComplete;
volatile uint8_t nfcTimerFired;

// Serial comms
#define MAX_STR_LENGTH 64
char wholeString[MAX_STR_LENGTH] = ""; // Entire input str from last 'return'
volatile uint8_t bCDCDataReceived_event;

#define UID_BUFFER_LENGTH 20
u08_t uid[UID_BUFFER_LENGTH];
size_t uidLength = 0;


// NFC
u08_t buf[140];					// TX/RX BUFFER FOR TRF7970A
u08_t enable = 0;
u08_t Tag_Count;
volatile u08_t i_reg = 0x01;             // INTERRUPT REGISTER
volatile u08_t irq_flag = 0x00;
u08_t rx_error_flag = 0x00;
s08_t rxtx_state = 1;           // USED FOR TRANSMIT RECEIVE BYTE COUNT
u08_t host_control_flag = 0;
u08_t stand_alone_flag = 1;


#define MAX_PASSWORD_LENGTH 64
#pragma DATA_SECTION(password,".infoD")
char password[MAX_PASSWORD_LENGTH]; // Store in info D (0x1800)

// Me (zero terminated)
const size_t passwordUidLength = 7;
const u08_t passwordUid[] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};

// Function declarations
void storePassword(char* newPassword);
void handleCDCDataReceived();
void lockPC(void);
void unlockPC(void);
uint8_t retInString (char* string);

/*
 * ======== main ========
 */
void main(void)
{
    WDT_A_hold(WDT_A_BASE); //Stop watchdog timer
	SFRIE1 |= WDTIE;		//Enable WDT interrupt


    // Minimum Vcore setting required for the USB API is PMM_CORE_LEVEL_2
#ifndef DRIVERLIB_LEGACY_MODE
    PMM_setVCore(PMM_CORE_LEVEL_2);
#else
    PMM_setVCore(PMM_BASE, PMM_CORE_LEVEL_2);
#endif

    initPorts();           // Config GPIOS for low-power (output low)
    initClocks(8000000);   // Config clocks. MCLK=SMCLK=FLL=8MHz; ACLK=REFO=32kHz
    USB_setup(TRUE,TRUE);  // Init USB & events; if a host is present, connect
    initButtons();
    Keyboard_init();       // Init keyboard report
    initKeyReports();

    initNFC();
    // wait until TRF7970A system clock started
	McuDelayMillisecond(2);
	// settings for communication with TRF7970A
	Trf7970CommunicationSetup();
	// Set Clock Frequency and Modulation
	Trf7970InitialSettings();
	// Re-configure the USART with this external clock
	Trf7970ReConfig();

	//Enable WDT interrupt
	//WDTCTL = WDT_ADLY_250;
	SFRIE1 |= WDTIE;

	START_NFC_TIMER;

    __enable_interrupt();  // Enable interrupts globally

    while (1)
    {

        switch(USB_connectionState())
        {
            // This case is executed while your device is enumerated on the
            // USB host
            case ST_ENUM_ACTIVE:

            	// Enter LPM0 w/interrupt, until a keypress occurs
                __bis_SR_register(LPM0_bits + GIE); // was LPM0 but think this disabled timers

                if (button1Pressed){
                	lockPC();
                	//START_NFC_TIMER;
                	button1Pressed = FALSE;
                }

                // disabled for now
                if (button2Pressed){
                	//START_NFC_TIMER;
                	button2Pressed = FALSE;
                }

                if (nfcTimerFired) {

                	GPIO_setOutputLowOnPin(LED_PORT, LED_PIN);

            		// Clear IRQ Flags before enabling TRF7970A
            		IRQ_CLR;
            		IRQ_ON;

            		ENABLE_TRF;

            		// Must wait at least 4.8 ms to allow TRF7970A to initialize.
            		__delay_cycles(40000);

            		Tag_Count = 0;

            		#ifdef ENABLE15693
            			 Iso15693FindTag();		// Scan for 15693 tags
            		#endif

            		#ifdef ENABLE14443A
            			  uidLength = Iso14443aFindTag(uid, UID_BUFFER_LENGTH);	// Scan for 14443A tags
            			  if (uidLength)
            			  {
            				  if (memcmp(uid, passwordUid, passwordUidLength) == 0) {
            					  GPIO_setOutputHighOnPin(LED_PORT, LED_PIN);
            					  unlockPC();
            					  PAUSE_NFC_TIMER;
            					  //STOP_NFC_TIMER;
            				  }
            			  }
            			  #endif

            		#ifdef ENABLE14443B
            			  Iso14443bFindTag();	// Scan for 14443B tags
            		#endif

            		//GPIO_setOutputLowOnPin(LED_PORT, LED_PIN);
            		nfcTimerFired=false;
                }

                // If true, some data is in the buffer; begin receiving a cmd
                if (bCDCDataReceived_event){
                	handleCDCDataReceived();
                }

                break;
            // These cases are executed while your device is disconnected from
            // the host (meaning, not enumerated); enumerated but suspended
            // by the host, or connected to a powered hub without a USB host
            // present.
            case ST_PHYS_DISCONNECTED:
            case ST_ENUM_SUSPENDED:
            case ST_PHYS_CONNECTED_NOENUM_SUSP:
                __bis_SR_register(LPM3_bits + GIE);
                _NOP();
                break;

            // The default is executed for the momentary state
            // ST_ENUM_IN_PROGRESS.  Usually, this state only last a few
            // seconds.  Be sure not to enter LPM3 in this state; USB
            // communication is taking place here, and therefore the mode must
            // be LPM0 or active-CPU.
            case ST_ENUM_IN_PROGRESS:
            default:;
        }
    }  //while(1)
} //main()

void lockPC() {

	sendLockPC();

}

void unlockPC() {

    uint16_t passwordLength = strlen(password);

	sendCtrlAltDel();
	__delay_cycles(8000000);

    uint8_t i;
    for (i=0; i<passwordLength; i++) {
    	Keyboard_write(password[i]);
        while(!keySendComplete);
        keySendComplete = FALSE;
        Keyboard_releaseAll();
        while(!keySendComplete);
        keySendComplete = FALSE;
    }

   	sendReturn();

}

void storePassword(char* newPassword) {

	  //char* ptrPassword = password;

	  FCTL3 = FWKEY;                            // Clear Lock bit
	  FCTL1 = FWKEY+ERASE;                      // Set Erase bit
	  *password = 0;                           // Dummy write to erase Flash seg
	  FCTL1 = FWKEY+WRT;                        // Set WRT bit for write operation


	  strcpy(password, newPassword);

	  FCTL1 = FWKEY;                            // Clear WRT bit
	  FCTL3 = FWKEY+LOCK;                       // Set LOCK bit
}

void handleCDCDataReceived() {

    // Holds the new addition to the string
    char pieceOfString[MAX_STR_LENGTH] = "";

    // Holds the outgoing string
    // char outString[MAX_STR_LENGTH] = "";

    // Add bytes in USB buffer to the string
    cdcReceiveDataInBuffer((uint8_t*)pieceOfString, MAX_STR_LENGTH, CDC0_INTFNUM); // Get the next piece of the string

    // Append new piece to the whole
    strcat(wholeString,pieceOfString);

    // Echo back the characters received
    cdcSend(pieceOfString);

    // Has the user pressed return yet?
    if (retInString(wholeString)){

    	storePassword(wholeString);

		// Prepare the outgoing string
		cdcSend("\r\nPassword set!\r\n\r\n");

		uint16_t i;

        // Clear the string in preparation for the next one
        for (i = 0; i < MAX_STR_LENGTH; i++){
            wholeString[i] = 0x00;
        }
    }
    bCDCDataReceived_event = FALSE;

}

/*
 * ======== retInString ========
 */
// This function returns true if there's an 0x0D character in the string; and if
// so, it trims the 0x0D and anything that had followed it.
uint8_t retInString (char* string)
{
    uint8_t retPos = 0,i,len;
    char tempStr[MAX_STR_LENGTH] = "";

    strncpy(tempStr,string,strlen(string));  // Make a copy of the string
    len = strlen(tempStr);

    // Find 0x0D; if not found, retPos ends up at len
    while ((tempStr[retPos] != 0x0A) && (tempStr[retPos] != 0x0D) &&
           (retPos++ < len)) ;

    // If 0x0D was actually found...
    if ((retPos < len) && (tempStr[retPos] == 0x0D)){
        for (i = 0; i < MAX_STR_LENGTH; i++){ // Empty the buffer
            string[i] = 0x00;
        }

        //...trim the input string to just before 0x0D
        strncpy(string,tempStr,retPos);

        //...and tell the calling function that we did so
        return ( TRUE) ;

    // If 0x0D was actually found...
    } else if ((retPos < len) && (tempStr[retPos] == 0x0A)){
        // Empty the buffer
        for (i = 0; i < MAX_STR_LENGTH; i++){
            string[i] = 0x00;
        }

        //...trim the input string to just before 0x0D
        strncpy(string,tempStr,retPos);

        //...and tell the calling function that we did so
        return ( TRUE) ;
    } else if (tempStr[retPos] == 0x0D){
        for (i = 0; i < MAX_STR_LENGTH; i++){  // Empty the buffer
            string[i] = 0x00;
        }
        // ...trim the input string to just before 0x0D
        strncpy(string,tempStr,retPos);
        // ...and tell the calling function that we did so
        return ( TRUE) ;
    } else if (retPos < len){
        for (i = 0; i < MAX_STR_LENGTH; i++){  // Empty the buffer
            string[i] = 0x00;
        }

        //...trim the input string to just before 0x0D
        strncpy(string,tempStr,retPos);

        //...and tell the calling function that we did so
        return ( TRUE) ;
    }

    return ( FALSE) ; // Otherwise, it wasn't found
}

/*
 * ======== UNMI_ISR ========
 */
#if defined(__TI_COMPILER_VERSION__) || (__IAR_SYSTEMS_ICC__)
#pragma vector = UNMI_VECTOR
__interrupt void UNMI_ISR (void)
#elif defined(__GNUC__) && (__MSP430__)
void __attribute__ ((interrupt(UNMI_VECTOR))) UNMI_ISR (void)
#else
#error Compiler not found!
#endif
{
        switch (__even_in_range(SYSUNIV, SYSUNIV_BUSIFG )) {
        case SYSUNIV_NONE:
                __no_operation();
                break;
        case SYSUNIV_NMIIFG:
                __no_operation();
                break;
        case SYSUNIV_OFIFG:
#ifndef DRIVERLIB_LEGACY_MODE
                UCS_clearFaultFlag(UCS_XT2OFFG);
                UCS_clearFaultFlag(UCS_DCOFFG);
                SFR_clearInterrupt(SFR_OSCILLATOR_FAULT_INTERRUPT);
#else
                UCS_clearFaultFlag(UCS_BASE, UCS_XT2OFFG);
                UCS_clearFaultFlag(UCS_BASE, UCS_DCOFFG);
                SFR_clearInterrupt(SFR_BASE, SFR_OSCILLATOR_FAULT_INTERRUPT);

#endif
                break;
        case SYSUNIV_ACCVIFG:
                __no_operation();
                break;
        case SYSUNIV_BUSIFG:
                // If the CPU accesses USB memory while the USB module is
                // suspended, a "bus error" can occur.  This generates an NMI.  If
                // USB is automatically disconnecting in your software, set a
                // breakpoint here and see if execution hits it.  See the
                // Programmer's Guide for more information.
                SYSBERRIV = 0;  // Clear bus error flag
                USB_disable();  // Disable
        }
}

//Released_Version_4_10_02
