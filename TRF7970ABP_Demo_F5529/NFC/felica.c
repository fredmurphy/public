/*
 * {felica.c}
 *
 * {FeliCa specific functions & Anti-collision}
 *
 * Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include "felica.h"
#include "trf7970.h"

//===============================================================

extern u08_t	buf[140];
extern u08_t	i_reg;
extern u08_t	irq_flag;
extern u08_t	stand_alone_flag;
extern s08_t	rxtx_state;
extern u08_t 	rx_error_flag;

void FindFelica(void)
{
	Trf7970TurnRfOn();
	
	Trf7970WriteIsoControl(0x1A);

	McuDelayMillisecond(6);

	FelicaPolling(0x00);						// do a complete anti-collision sequence as described

	// in ISO14443-3 standard for type A
	//Trf7970TurnRfOff();

	//Trf7970ResetIrqStatus();

}

//------------------------------------------------------//
//The Polling command can use any number of slots	//
//between 1 and 16 (00h - 0fh) witch is specified in the//
//'slotNo' variable.					//
//The slot time Ts is defined as (256 x 64 / fc) and the//
//delay Td between the Polling command and the first 	//
//timeslot is defined as (512 x 64 / fc).		//
//Td = 2.42ms = 2051 = 0x0803 for CounterSet()		//
//Ts = 1.21ms = 1025 = 0x0401 for CounterSet()		//
//------------------------------------------------------//
void FelicaPolling(u08_t slot_no)
{
	u08_t command[2], found = 0;
	u16_t k;
	char j = 1;
	
	//IRQ_CLR;			//PORT2 interrupt flag clear
	
	buf[0] 	= 0x8F;
	buf[1] 	= 0x91;		//send with CRC
	buf[2] 	= 0x3D;		//write continuous from 1D
	buf[3] 	= 0x00;
	buf[4] 	= 0x60;
	buf[5] 	= 0x06;
	buf[6] 	= POLLING;	//anti-collision command code
	buf[7] 	= 0xFF;
	buf[8] 	= 0xFF;
	buf[9] 	= 0x00;
	buf[10] = 0x00;
	
	Trf7970ResetIrqStatus();							//clearing IRQ (just in case)

	McuCounterSet();									// TimerA set
	COUNT_VALUE = COUNT_1ms * 30;						// 30ms, not 20ms
	IRQ_CLR;											// PORT2 interrupt flag clear (inside MSP430)
	IRQ_ON;

	Trf7970RawWrite(&buf[0], 11);	//writing to FIFO
	// Modification - EM
	rxtx_state = 0;

	//IRQ_ON;

	i_reg = 0x01;
	irq_flag = 0x00;
	START_COUNTER;										//	Starting Timeout

//	while(irq_flag == 0x00)
//	{
//	}													// wait for 0xA0 interrupt
//	RESET_COUNTER;

//	McuCounterSet();									// TimerA set
//	COUNT_VALUE = COUNT_1ms * 20;
//	START_COUNTER;										// start timer up mode

//	irq_flag = 0x00;

	while(irq_flag == 0x00)
	{
	}													// wait for end of TX interrupt
	RESET_COUNTER;

	McuCounterSet();									// TimerA set
	COUNT_VALUE = COUNT_1ms * 10;
	START_COUNTER;										// start timer up mode

	irq_flag = 0x00;

	while(irq_flag == 0x00)
	{
	}													// wait for end of TX interrupt
	RESET_COUNTER;

//	i_reg = 0x01;
	while(i_reg == 0x01)								// wait for RX complete
	{
		k++;

		if(k == 0xFFF0)
		{
			i_reg = 0x00;
			//rx_error_flag = 0x00;
		}
	}

	command[0] = RSSI_LEVELS;						// read RSSI levels
	Trf7970ReadSingle(command, 1);

	if( i_reg == 0xFF)
		{												 // if received block data in buffer
		if(stand_alone_flag == 1)
		{
			found = 1;
			#ifdef ENABLE_HOST
			//for(j = 12; j < 13; j++)
			{
				if (0xF0 == buf[11])
				UartSendCString("RC-S965 (FeliCa Lite)");
				else if (0xF1 == buf[11])
				UartSendCString("RC-S966 (FeliCa Lite S)");
				else if (0x0D == buf[11])
				UartSendCString("RC-S889 FeliCa");
				else if (0x01 == buf[11])
				UartSendCString("RC-S860 FeliCa");
			}
				UartPutCrlf();
				UartSendCString("NFC T3T  ID2:");
				//UartSendCString("NFCID2:");
				UartPutChar('[');
				//for(j = 1; j < rxtx_state; j++)
				for(j = 1; j < 10; j++)
				{
					UartPutByte(buf[j]);
				}
				UartPutChar(']');
				UartPutCrlf();
				UartSendCString("MFG CODE:    ");
				UartPutChar('[');
				for(j = 2; j < 4; j++)
				{
					UartPutByte(buf[j]);
				}
				UartPutChar(']');
				UartPutCrlf();
				UartSendCString("CIN:         ");
				UartPutChar('[');
				for(j = 4; j < 10; j++)
				{
					UartPutByte(buf[j]);
				}
				UartPutChar(']');
				UartPutCrlf();
				UartSendCString("PMm:         ");
				UartPutChar('[');
				for(j = 10; j < 18; j++)
				{
					UartPutByte(buf[j]);
				}
				UartPutChar(']');
				UartPutCrlf();
				//UartSendCString("ROM CODE:    ");
				//UartPutChar('[');
				//for(j = 11; j < 12; j++)
				//{
				//	UartPutByte(buf[j]);
				//}
				//UartPutChar(']');
				//UartPutCrlf();
				//UartSendCString("IC TYPE:     ");
				//UartPutChar('[');
				//UartPutChar(']');
				UartSendCString("RSSI LEVEL:  ");
				UartPutChar('[');
				UartPutByte(command[0]);		// RSSI levels
				UartPutChar(']');
				UartPutCrlf();
				UartPutCrlf();

			#endif

				rxtx_state = 1; 						//resetting buffer pointer

		}




	}


	if(found == 1)
	{
		LED_15693_ON;
		LED_14443B_ON;
	}
	else
	{
		//LED_15693_OFF;
		//LED_14443B_OFF;
	}
	Trf7970TurnRfOff();

	// clear any IRQs
	//Trf7970ResetIrqStatus();

}//FelicaPolling

